# Dead-simple scalable text notes

A very minimalistic note taking thing for Linux/Unix.

This has exactly what you need.
- Add entries with keywords
- Fuzzy search keywords, multiple keywords and substrings
- Recently accessed notes are listed first
- Everything locally stored
- Use your preferred text editor

![](https://files.mazie.rocks/txtdemo.webm)

#### These notes are an extension of your brain.
#### A search engine for your own past.
#### Incredibly fast to access.
#### Can be trusted to remain stable.
#### And quick to understand, because they are written by you, for yourself!

## Install and use

1. install `dmenu`
2. download the script `txt`
3. edit script, change `TERMINAL=urxvt` to your terminal
4. run the script from terminal to check if it works
5. create a keybind in your desktop environment / window manager (I use `mod+\`)
6. to synchronize between computers, I use `txt-sync` occasionally. You could also use git

Note down everything that would be nice to not forget:
- birthdays
- lifehacks
- ideas
- recipes
- copypasta
- link collections
- backups of text
- translations
- important chat messages
- password hints
- solutions to problems
- actual school notes
- travel log & other logs
- details that can't be forgotten


Anything that took more than a few minutes to figure out and might be useful again some day, I add to my notes.

I have over 1000 notes. Even so, I very rarely have issues finding stuff. Responsive fuzzy search is really powerful!
